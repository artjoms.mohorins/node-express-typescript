interface SellResult {
    sell: boolean;
    change: string;
  }