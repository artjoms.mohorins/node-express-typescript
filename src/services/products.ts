export const products = {
    productNames: [
      { name: 'Mars', price: 1.99 },
      { name: 'Snickers', price: 1.85 },
      { name: 'Cola', price: 1.33 },
      { name: 'Salt nuts', price: 2.5 }
    ]
  };

export function getPrice(productName: string) {
    const product = products.productNames.find(product => product.name === productName);
    return product?.price;
}

export function getChangeCoins(change: number): string {
  const coins = [100, 50, 20, 10, 5, 2, 1];
  let result = [];

  for (let i = 0; i < coins.length; i++) {
    while (change >= coins[i]) {
      change -= coins[i];
      result.push(coins[i]);
    }
  }

  return result.join(' ');
}

export function sellProduct(productName: string, coinsString: string): SellResult {
  const coinsArray = coinsString.split(' ').map(Number);
  const totalCoins = coinsArray.reduce((a, b) => a + b, 0);

  const product = products.productNames.find(prod => prod.name === productName);
  if (!product) {
    throw new Error('Product not found');
  }

  const price = product.price * 100; // convert to cents

  if (totalCoins < price) {
    return { sell: false, change: getChangeCoins(totalCoins) };
  } else {
    const change = totalCoins - price;
    return { sell: true, change: getChangeCoins(change) };
  }
}