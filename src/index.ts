import express, { Express, Request, Response } from 'express';
import dotenv from 'dotenv';
import { getChangeCoins, getPrice, products } from './services/products';
import { sellProduct } from './services/products';

dotenv.config();

const app: Express = express();
const port = process.env.PORT;

app.use(express.json()); 
app.use(express.urlencoded({ extended: true }));

app.get('/', (req: Request, res: Response) => {
  res.send(products);
});

app.post('/sell', (req: Request, res: Response) => {
  const { productName, coins } = req.body;

  try {
    const result = sellProduct(productName, coins);
    if (result.sell) {
      res.send({ message: "Product sold.", change: result.change });
    } else {
      res.status(400).send({ message: "You don't have enough money to buy this product.", change: result.change });
    }
  } catch (error) {
    res.status(404).send({ message: "This product does not exist." });
  }
});

app.listen(port, () => {
  console.log(`⚡️[server]: Server is running at http://localhost:${port}`);
});

