import { expect } from '@jest/globals';
import { getHeapCodeStatistics } from 'v8';
import { getPrice } from "../services/products";
import { getChangeCoins, sellProduct } from "../services/products";

test('Simple test', () => {
    //expect(1).toBe(1);
    expect(getChangeCoins(223)).toBe('100 100 20 2 1');
    });

test('Should return list of coins for change', () => {
    //expect(1).toBe(2);
    expect(getChangeCoins(223)).toBe('100 100 20 2 1');
    });
    
    
    test('Should get product price', () => {
    //expect(1).toBe(2);
    expect(getPrice('Mars')).toBe(1.99);
    });
    
    
    test('Should return true to sell product', () => {
    //expect(1).toBe(2);
    expect(sellProduct("Mars", "100 50 20 50 20 10")).toStrictEqual( { sell: true, change: "50 1"});
    });
    
    
    test('Should return false to sell product', () => {
    //expect(1).toBe(2);
    expect(sellProduct("Mars", "50 2 1")).toStrictEqual( { sell: false, change: "50 2 1"});
    });
    